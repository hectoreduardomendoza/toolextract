package com.tooling.toolextract.model

import java.math.BigDecimal
import java.util.*

data class FacturaRepetidaAppVM(var fecha: Date?, var idVendedor: Int, var nombreVendedor: String,
                                var idCliente: Int, var nombreCliente: String, var valor: BigDecimal,
                                var anulada: Int, var anuladaS: String) {
    constructor() : this(Date(), 0, "",
            0, "", BigDecimal(0.00),
            0, "")
}