package com.tooling.toolextract.model

import java.math.BigDecimal
import java.util.*

data class VentaDetalleModel(var id: Int, var idVenta: Int, var idBodega: Int,
                             var idProducto: Int, var unidades: BigDecimal, var precioCosto: BigDecimal,
                             var precioVenta: BigDecimal, var tipoPrecio: Int, var totalVenta: BigDecimal,
                             var totalCosto: BigDecimal, var fecha: Date?, var estado: Boolean,
                             var monedaExtranjera: BigDecimal, var factura: Int, var saltoFactura: Int,
                             var combo: Int, var precio2: BigDecimal, var total2: BigDecimal,
                             var descuentoUnitario: BigDecimal) {
    constructor() : this(0, 0, 0,
            0, BigDecimal(0.00), BigDecimal(0.00),
            BigDecimal(0.00), 0, BigDecimal(0.00),
            BigDecimal(0.00), Date(), false,
            BigDecimal(0.00), 0, 0,
            0, BigDecimal(0.00), BigDecimal(0.00),
            BigDecimal(0.00))
}