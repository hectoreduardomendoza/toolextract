package com.tooling.toolextract.model

import java.util.*

data class TurnoModel(var id: Int, var fechaApertura: Date?, var fechaCierre: Date?) {
    constructor() : this(0, Date(), Date())
}
