package com.tooling.toolextract.model

import java.math.BigDecimal

data class VentasModel(var factura: Int, var idProducto: Int, var nombre: String,
                       var unidades: Int, var precio: BigDecimal, var total: BigDecimal) {
    constructor() : this(0, 0, "", 0, BigDecimal(0.00), BigDecimal(0.00))
}