package com.tooling.toolextract.model

import java.math.BigDecimal

data class BoniProductoModel(var idProducto: Int, var nombre: String, var cantidadBoni: Int,
                             var precioTotal: BigDecimal, var precioUnitario: BigDecimal,
                             var precioCosto: BigDecimal, var cantidadPedir:Int) {
    constructor() : this(0, "", 0,
            BigDecimal(0.00), BigDecimal(0.00),
            BigDecimal(0.00),0)
}