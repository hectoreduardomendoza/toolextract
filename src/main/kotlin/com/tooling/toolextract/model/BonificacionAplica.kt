package com.tooling.toolextract.model

data class BonificacionAplica(var idPedido: Int, var idProducto: Int, var factura: Int) {
    constructor() : this(0, 0,0)
}