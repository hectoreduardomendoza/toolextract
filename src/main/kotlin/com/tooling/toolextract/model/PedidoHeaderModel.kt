package com.tooling.toolextract.model

import java.math.BigDecimal
import java.util.*

data class PedidoHeaderModel(var id: Int, var fecha: Date?, var valor: BigDecimal, var factura: Int) {
    constructor() : this(0, Date(), BigDecimal(0.00), 0)
}