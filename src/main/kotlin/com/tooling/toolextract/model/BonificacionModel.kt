package com.tooling.toolextract.model

data class BonificacionModel(var id: Int, var nombre: String, var cantidad: Int) {
    constructor() : this(0, "", 0)
}