package com.tooling.toolextract.model

import java.math.BigDecimal
import java.util.*

data class PedidoDetalleModel(var id: Int, var factura: Int, var idProducto: Int, var nombre: String,
                              var unidades: Int, var precio: BigDecimal, var total: BigDecimal, var fecha: Date?,
                              var tieneBoni: Boolean) {
    constructor() : this(0, 0, 0, "",
            0, BigDecimal(0.00), BigDecimal(0.00), Date(),
            false)
}