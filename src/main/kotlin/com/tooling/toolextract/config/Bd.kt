package com.tooling.toolextract.config

import java.sql.Connection
import java.sql.DriverManager

class Bd{
    fun getConexion(): Connection? {
        return getLocalDevelopMachineProperties()
    }

    fun getLocalDevelopMachineProperties(): Connection {
        val conexion = Conexion(Modo.USUARIO, Tipo.ANALISIS)

        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver")
        return DriverManager.getConnection(
                prepareStringConnection(
                        conexion.getServidor(),
                        conexion.getDataBase(),
                        conexion.getUser(),
                        conexion.getPassword()))
    }

    fun prepareStringConnection(servidor: String, baseDatos: String, usuario: String = "", clave: String = "") =
            "jdbc:sqlserver://$servidor;databaseName=$baseDatos;username=$usuario;password=$clave"
}