package com.tooling.toolextract.config

import org.slf4j.LoggerFactory
import java.io.FileInputStream
import java.util.*

class Conexion {

    private val logger = LoggerFactory.getLogger(this::class.java)

    var prop = Properties()
    var rutas = mapOf<Tipo, String>()

    constructor(modo: Modo, empresa: Tipo) {
        try {
            defineMap()
            val file = getScope(modo, empresa)
            var input = FileInputStream(file)
            prop.load(input)
        } catch (e: Exception) {
            logger.info("Error, no se encontró el archivo de properties")
        }
    }

    fun getFileName(nombre: Tipo) = rutas[nombre]

    fun getScope(modo: Modo, nombre: Tipo): String {
        val home = System.getProperty("user.home")

        val resultado = when (modo) {
            Modo.USUARIO -> "${home}/cplusserver/${getFileName(nombre)}"
            Modo.JAR -> "./${getFileName(nombre)}"
            else -> ""
        }
        return resultado
    }

    fun defineMap() {
        rutas = mapOf<Tipo, String>(
                Tipo.INVENTARIO to "inventario.properties",
                Tipo.PEDIDOS to "pedido.properties",
                Tipo.ANALISIS to "analisis.properties"
        )
    }

    fun getServidor() = prop.getProperty("datasource.servidor")

    fun getDataBase() = prop.getProperty("datasource.db")

    fun getUser() = prop.getProperty("datasource.user")

    fun getPassword() = prop.getProperty("datasource.password")
}