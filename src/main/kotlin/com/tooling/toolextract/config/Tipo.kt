package com.tooling.toolextract.config

enum class Tipo {
    INVENTARIO, PEDIDOS, ANALISIS
}