package com.tooling.toolextract

import com.tooling.toolextract.comando.Handler
import com.tooling.toolextract.repository.PedidoRepository
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
class ToolextractApplication

fun main(args: Array<String>) {
    runApplication<ToolextractApplication>(*args)
    val repo = PedidoRepository()

    val handler = Handler()
    handler.init()

}
