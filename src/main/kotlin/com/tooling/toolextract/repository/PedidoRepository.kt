package com.tooling.toolextract.repository

import com.tooling.toolextract.config.Bd
import com.tooling.toolextract.model.PedidoHeaderModel
import org.slf4j.LoggerFactory
import java.sql.ResultSet

class PedidoRepository {

    private val logger = LoggerFactory.getLogger(this::class.java)
    val bd = Bd()

    fun getByTurno(idTurno: Int): MutableList<PedidoHeaderModel> {
        val conexion = bd.getConexion()
        var lista = mutableListOf<PedidoHeaderModel>()

        try {
            val query = "Select ped_codigo, ped_fecha, " +
                    " ped_valor, ped_enviofacturado " +
                    " FROM ppedidos " +
                    " WHERE ped_turm_codigo=?" +
                    " ORDER BY ped_codigo "

            val cmd = conexion!!.prepareStatement(query)
            cmd.setInt(1, idTurno)
            val rs: ResultSet = cmd.executeQuery()

            while (rs.next()) {
                lista.add(PedidoHeaderModel(
                        rs.getInt("ped_codigo"),
                        rs.getTimestamp("ped_fecha"),
                        rs.getBigDecimal("ped_valor"),
                        rs.getInt("ped_enviofacturado")

                ))
            }
            conexion.close()
        } catch (e: Exception) {
            logger.info("error en listarTodos() ${e.message} ")
            throw  Exception("Error desde listar todos los pedidos")
        }
        return lista
    }
}