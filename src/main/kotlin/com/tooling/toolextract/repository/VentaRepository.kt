package com.tooling.toolextract.repository

import com.tooling.toolextract.config.Bd
import com.tooling.toolextract.model.VentasModel
import org.slf4j.LoggerFactory
import java.sql.ResultSet

class VentaRepository {

    private val logger = LoggerFactory.getLogger(this::class.java)
    val bd = Bd()

    fun getByTurno(idTurno: Int): MutableList<VentasModel> {
        val conexion = bd.getConexion()
        var lista = mutableListOf<VentasModel>()

        try {
            val query = " select ven_factura, vde_pre_pro_codigo, pro_nombre, " +
                    " vde_unidades, vde_precio, vde_total " +
                    " FROM ventas_detalle " +
                    " INNER JOIN ventas ON vde_ven_codigo=ven_codigo " +
                    " INNER JOIN producto ON pro_codigo=vde_pre_pro_codigo " +
                    " WHERE ven_turm_codigo=? " +
                    " ORDER BY ven_factura, vde_pre_pro_codigo "

            val cmd = conexion!!.prepareStatement(query)
            cmd.setInt(1, idTurno)
            val rs: ResultSet = cmd.executeQuery()

            while (rs.next()) {
                lista.add(VentasModel(
                        rs.getInt("ven_factura"),
                        rs.getInt("vde_pre_pro_codigo"),
                        rs.getString("pro_nombre"),
                        rs.getInt("vde_unidades"),
                        rs.getBigDecimal("vde_precio"),
                        rs.getBigDecimal("vde_total")
                ))
            }
            conexion.close()
        } catch (e: Exception) {
            logger.info("error en listar ventas ${e.message} ")
            throw  Exception("Error desde listar ventas")
        }
        return lista
    }
}