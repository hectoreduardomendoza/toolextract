package com.tooling.toolextract.repository

import com.tooling.toolextract.config.Bd
import com.tooling.toolextract.model.BoniProductoModel
import com.tooling.toolextract.model.BonificacionModel
import org.slf4j.LoggerFactory
import java.math.BigDecimal
import java.sql.ResultSet

class BonificacionRepository {

    private val logger = LoggerFactory.getLogger(this::class.java)
    val bd = Bd()

    fun get(): MutableList<BonificacionModel> {
        val conexion = bd.getConexion()
        var lista = mutableListOf<BonificacionModel>()

        try {
            val query = "SELECT bon_pro_codigo, pro_nombre, bon_cantidad " +
                    " FROM bonificaciones " +
                    " INNER JOIN producto ON pro_codigo = bon_pro_codigo " +
                    " WHERE pro_servicio=? " +
                    " ORDER BY bon_codigo"

            val cmd = conexion!!.prepareStatement(query)
            cmd.setInt(1, 0)
            val rs: ResultSet = cmd.executeQuery()

            while (rs.next()) {
                lista.add(BonificacionModel(
                        rs.getInt("bon_pro_codigo"),
                        rs.getString("pro_nombre"),
                        rs.getInt("bon_cantidad")
                ))
            }
            conexion.close()
        } catch (e: Exception) {
            logger.info("error en listar bonificaciones ${e.message} ")
            throw  Exception("Error desde listar bonificaciones")
        }
        return lista
    }

    fun tieneBoni(idProducto: Int): MutableList<BoniProductoModel> {

        var lista = mutableListOf<BoniProductoModel>()
        val conexion = bd.getConexion()

        try {
            val query = " SELECT bon_pro_codigoboni, pro_nombre, bon_cantidadboni, bon_pvtotal, bon_cantidad " +
            " FROM bonificaciones " +
            " INNER JOIN producto ON pro_codigo=bon_pro_codigoboni " +
            " WHERE bon_pro_codigo=? "

            val cmd = conexion!!.prepareStatement(query)
            cmd.setInt(1, idProducto)
            val rs: ResultSet = cmd.executeQuery()

            while (rs.next()) {
                lista.add(BoniProductoModel(
                        rs.getInt("bon_pro_codigoboni"),
                        rs.getString("pro_nombre"),
                        rs.getInt("bon_cantidadboni"),
                        rs.getBigDecimal("bon_pvtotal"),
                        BigDecimal(0.00),
                        BigDecimal(0.00),
                        rs.getInt("bon_cantidad")
                ))
            }

            for (x in lista) {
                x.precioUnitario = x.precioTotal / BigDecimal(x.cantidadBoni)
            }

            rs.close()
            conexion.close()
        } catch (e: Exception) {
            logger.info("error en listar bonificaciones ${e.message} ")
            throw  Exception("Error desde tieneBoni()")
        }
        return lista
    }
}