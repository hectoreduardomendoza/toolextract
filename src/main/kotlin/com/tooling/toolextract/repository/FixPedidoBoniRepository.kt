package com.tooling.toolextract.repository

import com.tooling.toolextract.config.Bd
import com.tooling.toolextract.model.VentaDetalleModel
import org.slf4j.LoggerFactory
import java.math.BigDecimal
import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.Timestamp
import java.util.*

class FixPedidoBoniRepository {

    private val logger = LoggerFactory.getLogger(this::class.java)
    val bd = Bd()

    fun fix(idFactura: Int, unidades: BigDecimal, idBoni: Int): String {
        val conexion = bd.getConexion()

        var mensaje: String = ""
        var existencia: BigDecimal

        conexion!!.autoCommit = false
        try {
            existencia = getExistencia(conexion, idBoni)
            if (existencia < BigDecimal(-1.00)) {
                mensaje = "error no se encontraron existencias o están en valores negativos"
            } else {
                if (existencia >= unidades) {
                    //obtener un detalle de ejemplo
                    //vde_ven_codigo, vde_exi_bod_codigo
                } else {
                    mensaje = "no es posible surtir esa cantidad, solo se cuentan con $existencia unidades"

                }
            }
            // borrarProductosPedidos(conexion, idGrupo)
            // borrarBonificaciones(conexion, idGrupo)

            // totalUnidades = insertarProductos(conexion, grupo.productos)

            // if (grupo.bonificacion != null) {
            //     insertarBonificacion(conexion, grupo.bonificacion!!)
            // }

            // updateHeader(conexion, totalUnidades, grupo.totalPedidoGrupo, idGrupo)
            // idPedido = getIdPedido(conexion, idGrupo)
            // totalPedidoDetalle = getTotal(conexion, idPedido)
            // totalGlobal = totalPedidoDetalle + grupo.totalPedidoGrupo
            //  monedaExtranjera = iva.calcularFixed(totalGlobal, 2)
            //  updatePedido(conexion, totalGlobal, monedaExtranjera, idPedido)
            conexion.commit()
            conexion.close()
        } catch (e: Exception) {
            conexion.rollback()
            conexion.close()
            logger.info(e.message)
            logger.info("Error no pudimos actualizar la factura ${idFactura}")
            throw Exception("Error desde fix pedido ")
        }
        return mensaje
    }

    fun getExistencia(con: Connection, idProducto: Int): BigDecimal {
        var existencia = BigDecimal(-1.00)

        val query = "SELECT exi_canactual FROM existencias WHERE exi_pro_codigo=?"
        val cmd = con!!.prepareStatement(query)
        cmd.setInt(1, idProducto)
        val rs: ResultSet = cmd.executeQuery()
        if (rs.next()) {
            existencia = rs.getBigDecimal("exi_canactual")
        }
        rs.close()
        cmd.close()

        return existencia
    }

    fun getDetalleEjemplo(idFactura: Int) {
        val query = "SELECT vde_codigo, vde_ven_codigo, vde_exi_bod_codigo, vde_pre_pro_codigo, " +
                " vde_unidades, vde_costo, vde_precio, vde_pre_prs_codigo, " +
                " vde_total, vde_costototal, vde_fecha, vde_estado, " +
                " vde_monextran, vde_factura, vde_saltofactura, vde_combo, " +
                " vde_precio2, vde_total2, vde_descunit " +
                " FROM ventas_detalle " +
                " WHERE vde_ven_factura =? "
    }

    fun setQueryDetalle() = "INSERT into ventas_detalle( " +
            "vde_ven_codigo, vde_exi_bod_codigo, " +//1
            "vde_pre_pro_codigo, vde_unidades, vde_costo, " +//2
            "vde_precio, vde_pre_prs_codigo, vde_total, " +//3
            "vde_costototal, vde_fecha, vde_estado, " +//4
            "vde_monextran, vde_factura, vde_saltofactura, " +//5
            "vde_combo, vde_precio2, vde_total2, " +//6
            "vde_descunit) " + //7
            "VALUES(?,?,?,?,?, " +
            "?,?,?,?,?, " +
            "?,?,?,?,?, " +
            "?,?,? " +
            ");"


    fun setPreparedStatementDetalle(idVenta: Int, el: VentaDetalleModel, cmd: PreparedStatement): PreparedStatement {
        val date = Date()

        //1
        cmd.setInt(1, idVenta)
        cmd.setInt(2, el.idBodega)

        //2
        cmd.setInt(3, el.idProducto)
        cmd.setBigDecimal(4, el.unidades)
        cmd.setBigDecimal(5, el.precioCosto)

        //3
        cmd.setBigDecimal(6, el.precioVenta)
        cmd.setInt(7, el.tipoPrecio)
        cmd.setBigDecimal(8, el.totalVenta)

        //4
        cmd.setBigDecimal(9, el.totalCosto)
        cmd.setTimestamp(10, Timestamp(date.time))
        cmd.setBoolean(11, el.estado)

        //5
        cmd.setBigDecimal(12, el.monedaExtranjera)
        cmd.setInt(13, el.factura)
        cmd.setInt(14, el.saltoFactura)

        //6
        cmd.setInt(15, 0)
        cmd.setBigDecimal(16, el.precio2)
        cmd.setBigDecimal(17, el.total2)

        //7
        cmd.setBigDecimal(18, el.descuentoUnitario)

        return cmd
    }

    /*
    private fun getIdPedido(con: Connection, idPedidoGrupo: Int): Int {
        val query = " SELECT pdg_ped_codigo FROM ppedidos_grupo WHERE pdg_codigo=?"
        var idPedido = 0
        val cmd = con!!.prepareStatement(query)
        cmd.setInt(1, idPedidoGrupo)

        val rs: ResultSet = cmd.executeQuery()
        if (rs.next()) {
            idPedido = rs.getInt("pdg_ped_codigo")
        }

        cmd.close()
        return idPedido
    }
     */
}