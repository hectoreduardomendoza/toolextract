package com.tooling.toolextract.repository

import com.tooling.toolextract.config.Bd
import com.tooling.toolextract.model.FacturaRepetidaAppVM
import org.slf4j.LoggerFactory
import java.sql.ResultSet

class FacturasRepetidasAppRepository {

    private val logger = LoggerFactory.getLogger(this::class.java)
    val bd = Bd()

    fun getByTurno(idTurno: Int): MutableList<FacturaRepetidaAppVM> {

        val conexion = bd.getConexion()
        var lista = mutableListOf<FacturaRepetidaAppVM>()

        try {
            val query = "SELECT ven_fecha, ven_vnd_codigo, vnd_nombre,  " +
                    " ven_cli_codigo, cli_nombre, cli_apellidos, " +
                    " ven_valor, ven_anulada " +
                    " FROM ventas " +
                    " INNER JOIN vendedor ON vnd_codigo=ven_vnd_codigo " +
                    " INNER JOIN clientes ON cli_codigo = ven_cli_codigo " +
                    " WHERE ven_turm_codigo =? " +
                    " AND ven_cli_codigo in( " +
                    " SELECT ven_cli_codigo from ventas " +
                    " WHERE ven_turm_codigo=? " +
                    " GROUP BY ven_cli_codigo " +
                    " HAVING COUNT(ven_cli_codigo)>1) " +
                    " ORDER BY ven_cli_codigo, ven_fecha, ven_vnd_codigo "

            val cmd = conexion!!.prepareStatement(query)
            cmd.setInt(1, idTurno)
            cmd.setInt(2, idTurno)
            val rs: ResultSet = cmd.executeQuery()

            while (rs.next()) {
                lista.add(FacturaRepetidaAppVM(
                        rs.getTimestamp("ven_fecha"),
                        rs.getInt("ven_vnd_codigo"),
                        rs.getString("vnd_nombre").trim(),
                        rs.getInt("ven_cli_codigo"),
                        "${rs.getString("cli_nombre").trim()}  ${rs.getString("cli_apellidos")}",
                        rs.getBigDecimal("ven_valor"),
                        rs.getInt("ven_anulada"),
                        ""
                ))
            }
            conexion.close()
        } catch (e: Exception) {
            logger.info("Error en buscar facturas repetidas por la App ${e.message} ")
            throw  Exception("Error desde listar bonificaciones")
        }
        return lista
    }

}