package com.tooling.toolextract.repository

import com.tooling.toolextract.config.Bd
import com.tooling.toolextract.model.PedidoDetalleModel
import org.slf4j.LoggerFactory
import java.sql.ResultSet

class PedidoDetalleRepository {

    private val logger = LoggerFactory.getLogger(this::class.java)
    val bd = Bd()

    fun getByTurno(idTurno: Int): MutableList<PedidoDetalleModel> {
        val conexion = bd.getConexion()
        var lista = mutableListOf<PedidoDetalleModel>()

        try {
            val query = "SELECT pde_ped_codigo, ped_enviofacturado, pde_pre_pro_codigo, " +
                    " pro_nombre, pde_unidades, pde_precio, pde_total, pde_fecha " +
                    " FROM ppedidos_detalle" +
                    " INNER JOIN producto ON pro_codigo = pde_pre_pro_codigo " +
                    " INNER JOIN ppedidos ON ped_codigo = pde_ped_codigo" +
                    " WHERE ped_turm_codigo =? " +
                    " ORDER BY pde_ped_codigo"

            val cmd = conexion!!.prepareStatement(query)
            cmd.setInt(1, idTurno)
            val rs: ResultSet = cmd.executeQuery()

            while (rs.next()) {
                lista.add(PedidoDetalleModel(
                        rs.getInt("pde_ped_codigo"),
                        rs.getInt("ped_enviofacturado"),
                        rs.getInt("pde_pre_pro_codigo"),
                        rs.getString("pro_nombre"),
                        rs.getInt("pde_unidades"),
                        rs.getBigDecimal("pde_precio"),
                        rs.getBigDecimal("pde_total"),
                        rs.getTimestamp("pde_fecha"),
                        false
                ))
            }
            conexion.close()
        } catch (e: Exception) {
            logger.info("error en listar pedido detalle() ${e.message} ")
            throw  Exception("Error desde listar pedido detalle")
        }
        return lista
    }
}