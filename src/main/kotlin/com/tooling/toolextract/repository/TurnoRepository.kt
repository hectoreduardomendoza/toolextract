package com.tooling.toolextract.repository

import com.tooling.toolextract.config.Bd
import com.tooling.toolextract.model.TurnoModel
import org.slf4j.LoggerFactory
import java.sql.ResultSet

class TurnoRepository {

    private val logger = LoggerFactory.getLogger(this::class.java)
    val bd = Bd()

    fun getTurnos(size: Int): MutableList<TurnoModel> {

        val conexion = bd.getConexion()
        var lista = mutableListOf<TurnoModel>()

        try {
            val query = " SELECT  top($size) turm_codigo, turm_fechaapertura, turm_fechacierra " +
                    " FROM turnos_movi ORDER By turm_codigo DESC "

            val cmd = conexion!!.prepareStatement(query)
            val rs: ResultSet = cmd.executeQuery()

            while (rs.next()) {
                lista.add(TurnoModel(
                        rs.getInt("turm_codigo"),
                        rs.getTimestamp("turm_fechaapertura"),
                        rs.getTimestamp("turm_fechacierra")
                ))
            }
            rs.close()
            cmd.close()
            conexion.close()
        } catch (e: Exception) {
            logger.info("error en listarTodos() ${e.message} ")
            throw  Exception("Error desde listar todos los pedidos")
        }
        return lista
    }
}