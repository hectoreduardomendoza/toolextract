package com.tooling.toolextract.comando

import com.tooling.toolextract.model.*
import com.tooling.toolextract.repository.BonificacionRepository
import com.tooling.toolextract.repository.FixPedidoBoniRepository
import com.tooling.toolextract.repository.TurnoRepository

class Handler {

    val repoBoni = BonificacionRepository()

    fun init() {
        val repoTurno = TurnoRepository()


        val pedidosHandler = PedidosHandler()
        val pedidoDetalleHandler = PedidoDetalleHandler()
        val ventaHandler = VentaHandler()
        val boniHandler = BonificacionHandler()
        val factRepHandler = FacturaRepetidaAppHandler()
        var listaComandos: MutableList<String>

        val turnos: MutableList<TurnoModel>
        val bonificaciones: MutableList<BonificacionModel>
        val pedidos: MutableList<PedidoHeaderModel>
        val detalles: MutableList<PedidoDetalleModel>
        val ventas: MutableList<VentasModel>

        val repetidas: MutableList<FacturaRepetidaAppVM>
        var bonifCan = mutableListOf<BonificacionAplica>()

        turnos = repoTurno.getTurnos(10)

        println("Los turnos encontrados son:")
        for (el in turnos) {
            println("${el.id} - ${el.fechaApertura} - ${el.fechaCierre} ")
        }

        println("Ingrese el turno a obtener pedidos")
        var turno = readLine()
        cls()

        println("Obteniendo pedidos del turno: ${turno} ...")
        pedidosHandler.init(turno!!.toInt())
        pedidos = pedidosHandler.getList()
        println("Se obtuvieron ${pedidosHandler.getSize()} pedidos.")


        println("Obteniendo detalle de los pedidos")
        pedidoDetalleHandler.init(turno!!.toInt())
        println("Se obtuvieron  ${pedidoDetalleHandler.getSize()} registros de detalle")

        print("Obteniendo las ventas...")
        ventaHandler.init(turno!!.toInt())
        ventas = ventaHandler.getList()
        println("Se obtuvieron  ${ventaHandler.getSize()} registros de ventas")
        println("Obteniendo bonificaciones")

        bonificaciones = boniHandler.loadFixed()

        println("Se obtuvieron ${bonificaciones.size} registros de bonificaciones")

        detalles = pedidoDetalleHandler.getList()
        for (el in detalles) {
            var res = bonificaciones.find { it.id === el.idProducto }
            if (res != null) {
                if (el.unidades >= res.cantidad) {
                    bonifCan.add(BonificacionAplica(el.id, el.idProducto, el.factura))
                }
            }
        }

        for (el in bonifCan) {
            println("${el}")
        }

        println("Posibles pedidos con bonificacion=${bonifCan.size}")

        factRepHandler.init(turno!!.toInt())
        println("${factRepHandler.getSize()} facturas repetidas")
        factRepHandler.show()

        print("?")
        var comando = readLine()

        while (!comando.equals("exit")) {
            listaComandos = comando?.let { splitCadena(it) }!!

            if (listaComandos.size == 1) {
                when (comando!!.trim()) {
                    "cls" -> cls()
                }
            }

            if (listaComandos.size == 3) {
                when (listaComandos[0]) {
                    "get" -> getComando(listaComandos, pedidos, ventas, detalles)
                }
            }

            if (listaComandos.size == 5) {
                /*println("${listaComandos[0]} ${listaComandos[1]}")
                println("Se le agregán ${listaComandos[4]} unidades de la boni ${listaComandos[3]} a la factura ${listaComandos[2]}")*/
                when (listaComandos[0]) {
                    "fix" -> {
                        when (listaComandos[1]) {
                            "pedido" -> {
                                //println("fix pedido")
                                val fix = FixPedidoBoniRepository()
                                println(fix.fix(listaComandos[2].toInt(), listaComandos[4].toBigDecimal(), listaComandos[3].toInt()))
                            }
                        }
                    }
                }
            }

            print("?")
            comando = readLine()
        }
        println("saliendo del programa")

    }

    fun cls() {
        ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor()
    }

    fun getComando(comandos: MutableList<String>, lista: MutableList<PedidoHeaderModel>, ventas: MutableList<VentasModel>,
                   detalle: MutableList<PedidoDetalleModel>) {
        when (comandos[1]) {
            "pedido" -> getPedido(comandos, lista, ventas, detalle)
        }
    }

    fun getPedido(comandos: MutableList<String>, pedidos: MutableList<PedidoHeaderModel>, ventas: MutableList<VentasModel>,
                  detalle: MutableList<PedidoDetalleModel>) {
        getPedidoCompleto(comandos[2].toInt(), pedidos, ventas, detalle)
    }

    fun getPedidoCompleto(idPedido: Int, pedidos: MutableList<PedidoHeaderModel>,
                          ventas: MutableList<VentasModel>, detalle: MutableList<PedidoDetalleModel>) {

        var detallesPedido = mutableListOf<PedidoDetalleModel>()
        var oneVenta = mutableListOf<VentasModel>()


        val ANSI_RESET = "\u001B[0m";
        val ANSI_BLACK = "\u001B[30m";
        val ANSI_RED = "\u001B[31m";
        val ANSI_GREEN = "\u001B[32m";
        val ANSI_YELLOW = "\u001B[33m";
        val ANSI_BLUE = "\u001B[34m";
        val ANSI_PURPLE = "\u001B[35m";
        val ANSI_CYAN = "\u001B[36m";
        val ANSI_WHITE = "\u001B[37m";
        val ANSI_BOLD = "\u001b[1m"

        /*val BLACK_BRIGHT = "\u033[0;90m";  // BLACK
        val RED_BRIGHT = "\u033[0;91m";    // RED
        val GREEN_BRIGHT = "\u033[0;92m";  // GREEN
        val YELLOW_BRIGHT = "\u033[0;93m"; // YELLOW
        val BLUE_BRIGHT = "\u033[0;94m";   // BLUE
        val PURPLE_BRIGHT = "\u033[0;95m"; // PURPLE
        val CYAN_BRIGHT = "\u033[0;96m";   // CYAN
        val WHITE_BRIGHT = "\u033[0;97m"  // WHITE
            */
        //println("${ANSI_RED} Rojo ${ANSI_WHITE}")

        println("devolviendo el pedido $idPedido para análisis")

        val item = pedidos.find({ x -> x.id == idPedido })
        cls()
        println("pedido=${item!!.id}  -  fecha=${item!!.fecha} - valor=${item.valor}")
        if (item.factura == 0) println("sin facturar") else println("factura=${item.factura}")

        for (el in detalle) {
            if (el.id == item.id) {
                detallesPedido.add(el)
            }
        }

        println()
        println()
        for (a in detallesPedido) {
            var listaBonis = mutableListOf<BoniProductoModel>()
            listaBonis = repoBoni.tieneBoni(a.idProducto)
            var boni = if (listaBonis.size > 0) "*" else ""
            println("${a.fecha} -  ${boni}${a.idProducto} -  ${a.nombre} - ${a.unidades} - Q${a.precio} - Q${a.total}")
            for (b in listaBonis) {
                //office
                println("${ANSI_BOLD}${ANSI_YELLOW}     *****${b.idProducto} -  ${b.nombre}  pedir=${b.cantidadPedir} para dar=${b.cantidadBoni} - Q${b.precioTotal} - Q${b.precioUnitario} - Q${b.precioCosto}  ${ANSI_WHITE}")

                //home
                //println("================================================================================================")
                //println("     *****${b.idProducto} -  ${b.nombre}  pedir=${b.cantidadPedir} para dar=${b.cantidadBoni} - Q${b.precioTotal} - Q${b.precioUnitario} - Q${b.precioCosto}")
                //println("================================================================================================")

            }
        }
        //office
        println("${ANSI_BOLD}${ANSI_RED}${detallesPedido.size}${ANSI_WHITE} productos + bonis")

        //home
        //println("${detallesPedido.size} productos + bonis")


        if (item.factura > 0) {
            for (el in ventas) {
                if (el.factura == item.factura) {
                    oneVenta.add(el)
                }
            }
        }

        println()
        println()
        for (a in oneVenta) {
            println("${a.idProducto} -  ${a.nombre} - ${a.unidades} - Q${a.precio} - Q${a.total}")
        }
        //office
        println("${ANSI_BOLD}${ANSI_RED}${oneVenta.size}${ANSI_WHITE} productos ")

        //home
        //println("${oneVenta.size} productos ")

        println()
        println()
    }

    fun splitCadena(query: String): MutableList<String> {
        val comandos = query.trim().split(" ").filter { s -> s.isNotEmpty() }
        return comandos.toMutableList()
    }

}