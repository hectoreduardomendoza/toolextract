package com.tooling.toolextract.comando

import com.tooling.toolextract.model.PedidoHeaderModel
import com.tooling.toolextract.repository.PedidoRepository
import java.io.FileWriter

class PedidosHandler {

    val repo = PedidoRepository()
    var lista = mutableListOf<PedidoHeaderModel>()

    fun init(idTurno: Int) {
        val charPool: List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')

        lista = repo.getByTurno(idTurno)

        val randomString = (1..5)
                .map { i -> kotlin.random.Random.nextInt(0, charPool.size) }
                .map(charPool::get)
                .joinToString("");

        val csvWriter = FileWriter("pedidos${idTurno}${randomString}.csv")

        csvWriter.append("idPedido;fecha;valor;factura\n")
        for (el in lista) {
            csvWriter.append("${el.id};${el.fecha};${el.valor}; ${el.factura}\n")
        }

        csvWriter.flush()
        csvWriter.close()

    }

    fun getSize() = lista.size

    fun getList() = lista

}