package com.tooling.toolextract.comando

import com.tooling.toolextract.model.VentasModel
import com.tooling.toolextract.repository.VentaRepository
import java.io.FileWriter

class VentaHandler {

    val repo = VentaRepository()
    var lista = mutableListOf<VentasModel>()
    var ventas = mutableListOf<Int>()

    fun init(idTurno: Int) {
        val charPool: List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')

        lista = repo.getByTurno(idTurno)

        for (el in lista) {
            ventas.add(el.factura)
        }

        println("${ventas.distinct().size} facturas")
        val randomString = (1..5)
                .map { i -> kotlin.random.Random.nextInt(0, charPool.size) }
                .map(charPool::get)
                .joinToString("");

        val csvWriter = FileWriter("ventas${idTurno}${randomString}.csv")

        csvWriter.append("factura;id;nombre;unidades;precio;total\n")
        for (el in lista) {
            csvWriter.append("${el.factura};${el.idProducto}; ${el.nombre};${el.unidades};${el.precio};${el.total}\n")
        }
        csvWriter.flush()
        csvWriter.close()
    }

    fun getSize() = lista.size

    fun getList() = lista
}