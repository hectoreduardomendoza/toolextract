package com.tooling.toolextract.comando

import com.tooling.toolextract.model.PedidoDetalleModel
import com.tooling.toolextract.repository.BonificacionRepository
import com.tooling.toolextract.repository.PedidoDetalleRepository
import java.io.FileWriter

class PedidoDetalleHandler {

    val repo = PedidoDetalleRepository()
    val repoBoni=BonificacionRepository()

    var lista = mutableListOf<PedidoDetalleModel>()

    fun init(idTurno: Int) {
        val charPool: List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')

        lista = repo.getByTurno(idTurno)

        val randomString = (1..5)
                .map { i -> kotlin.random.Random.nextInt(0, charPool.size) }
                .map(charPool::get)
                .joinToString("");

        val csvWriter = FileWriter("pedidosDetalle${idTurno}${randomString}.csv")

        csvWriter.append("idPedido;factura;id;producto;unidades;precio;total;fecha;boni\n")
        for (el in lista) {
            csvWriter.append("${el.id};${el.factura};${el.idProducto}; ${el.nombre};${el.unidades};${el.precio};${el.total};${el.fecha}\n")
        }
        csvWriter.flush()
        csvWriter.close()
    }

    fun getSize() = lista.size

    fun getList()= lista
}