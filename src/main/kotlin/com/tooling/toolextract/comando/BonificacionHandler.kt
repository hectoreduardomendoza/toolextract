package com.tooling.toolextract.comando

import com.tooling.toolextract.model.BonificacionModel
import com.tooling.toolextract.repository.BonificacionRepository
import java.io.FileWriter

class BonificacionHandler {

    val repo = BonificacionRepository()
    var lista = mutableListOf<BonificacionModel>()

    fun init() {
        lista = repo.get()
    }

    fun loadFixed(): MutableList<BonificacionModel> {
        lista.add(BonificacionModel(1, "ACEITE AGUACATINA 120ML FDO  100 UNI.", 12))
        lista.add(BonificacionModel(2, "ACEITE BABY CHIC 115 ML FDO  100 UNI.", 12))
        lista.add(BonificacionModel(3, "ACEITE OLIVO PREDILECTO 120 ML 48 UNI", 12))
        lista.add(BonificacionModel(99, "CEREAL CORN FLAKES  KELLOGGS 150G FDO 28 UNI", 12))
        lista.add(BonificacionModel(104, "LAMPARA AHORADORA FG 25W FARDO 50UNI", 12))

        lista.add(BonificacionModel(178, "DETERGENTE. GALLO 1 KG. FARDO 10 UNI", 10))
        lista.add(BonificacionModel(179, "DETERGENTE. GALLO 250GS. FARDO 30 UNI", 30))
        lista.add(BonificacionModel(197, "DICLOFENCO + NEUROTROPAS CAJA DE 5 BLISTER", 5))
        lista.add(BonificacionModel(265, "GRANOS DE ELOTITOS HERDEZ LATA 220G CHAROLA 24UNI", 12))
        lista.add(BonificacionModel(266, "GRANOS DE ELOTITOS HERDEZ LATA 400G CHAROLA 24UNI", 12))

        lista.add(BonificacionModel(310, "LAMPARA AHORRADORA LIMON 25 WTS. FDO 50UNI.", 12))
        lista.add(BonificacionModel(328, "NAUSEOL CAJA 10 BLISTER  100 UNI", 5))
        lista.add(BonificacionModel(332, "NEUROBION CAJA 24 BLISTER", 12))
        lista.add(BonificacionModel(353, "PAPEL HIGINICO ENCANTO 1000  HJS 12 PAQ.4 ROLLOS", 1))
        lista.add(BonificacionModel(355, "PAPEL HIGINICO ENCANTO ECOLOGICO 12 PAQ 4 ROLLOS 1000 HJS.", 1))

        lista.add(BonificacionModel(368, "POMADA CLOTRIPLEX  CAJA 40 UNIDADES", 20))
        lista.add(BonificacionModel(392, "SERTAL COMPUESTO CJA 20 BLISTER", 5))
        lista.add(BonificacionModel(448, "TOALLA  SANITARIA STAYFREE PAQ. 10 TOALLAS FDO 24 PAQ.", 24))
        lista.add(BonificacionModel(449, "TOALLA SABA BUENAS NOCHES PAQ. 8 UNI CJA  6 PAQ", 6))
        return lista
    }

    fun createFile() {
        val charPool: List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')

        val randomString = (1..5)
                .map { i -> kotlin.random.Random.nextInt(0, charPool.size) }
                .map(charPool::get)
                .joinToString("");

        val csvWriter = FileWriter("bonificaciones${randomString}.csv")

        csvWriter.append("idProducto;nombre;minimo\n")
        for (el in lista) {
            csvWriter.append("${el.id};${el.nombre};${el.cantidad}\n")
        }
        csvWriter.flush()
        csvWriter.close()
    }

    fun getSize() = lista.size

}