package com.tooling.toolextract.comando

import com.tooling.toolextract.model.FacturaRepetidaAppVM
import com.tooling.toolextract.repository.FacturasRepetidasAppRepository

class FacturaRepetidaAppHandler {
    val repo = FacturasRepetidasAppRepository()
    var lista = mutableListOf<FacturaRepetidaAppVM>()

    fun init(idTurno: Int) {
        lista = repo.getByTurno(idTurno)
        for (a in lista) {
            if (a.anulada == 1) {
                a.anuladaS = "Anulada"
            }
        }
    }

    fun getSize() = lista.size

    fun getList() = lista

    fun show() {
        var i = 1
        for (el in lista) {
            println("${i} - ${el.idCliente} - ${el.fecha} - ${el.idVendedor} - ${el.nombreVendedor} - ${el.valor} - ${el.anuladaS} ")
            i++
        }
    }
}
